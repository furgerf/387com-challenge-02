//
//  GraphView.swift
//  WeatherApplication
//
//  Created by Fabian Furger on 11/15/14.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class GraphView: UIView {
    // data
    var time:[String] = [String]()
    var temp:[CGFloat] = [CGFloat]()
    var percip:[CGFloat] = [CGFloat]()

    // method...
    override func drawRect(rect: CGRect) {
        println(temp)
        
        // some sizes...
        let context:CGContext = UIGraphicsGetCurrentContext()
        let width:CGFloat = self.bounds.width
        let step:CGFloat = width / 9
        let height:CGFloat = self.bounds.height
        let xOffset:CGFloat = step
        let graphHeight:CGFloat = height - 12
        
        // string attributes
        let font = UIFont.systemFontOfSize(8)
        let percipitationColor = UIColor(red: 0, green: 0.3, blue: 0.8, alpha: 1)
        let percipitationColorComponents = CGColorGetComponents(percipitationColor.CGColor)
        let tempColor = UIColor(red: 0.4, green: 0.6, blue: 0.2, alpha: 1)
        let tempColorComponents = CGColorGetComponents(tempColor.CGColor)
        
        ///////////////
        // DRAW GRID //
        ///////////////
        var dash:[CGFloat] = [2, 2]
        CGContextSetLineDash(context, 0, dash, 2)
        for (var i = 0; i < 8; i++)
        {
            let x:CGFloat = xOffset + step * (CGFloat(i))
            CGContextMoveToPoint(context, x, 0)
            CGContextAddLineToPoint(context, x, graphHeight - 6)
            
            if i > 7 - time.count {
                (time[i - 8 + time.count] as NSString).drawAtPoint(CGPoint(x: x - 10, y: graphHeight), withAttributes: [NSFontAttributeName:font])
            }
        }
        CGContextAddLineToPoint(context, xOffset, graphHeight - 6)
        CGContextStrokePath(context)
        
        ////////////////////////
        // DRAW PERCIPITATION //
        ////////////////////////
        // calculations - percipitations
        let sortedPercip:[CGFloat] = percip.sorted({(t1:CGFloat, t2:CGFloat) -> Bool in return t1 < t2})
        if sortedPercip.last != 0 {
            let minPercip:CGFloat = 0
            var maxPercip = sortedPercip.last! + 2
            maxPercip -= maxPercip % 2
            let deltaPercip = maxPercip - minPercip
            let minPercipAndAThird = minPercip + (maxPercip - minPercip) / 3
            let minPercipAndTwoThirds = minPercip + (maxPercip - minPercip) / 3 * 2
            
            // calculations - graph coordinates
            let minPercipY:CGFloat = graphHeight - 6
            let maxPercipY:CGFloat = 3
            let deltaPercipY = maxPercipY - minPercipY
            let minPercipAndAThirdY = minPercipY + (maxPercipY - minPercipY) / 3
            let minPercipAndTwoThirdsY = minPercipY + (maxPercipY - minPercipY) / 3 * 2
            
            // draw scale
            let yOffset:CGFloat = -6 // offset to center text on y-coordinate
            NSString(format: "%.02f", Float(minPercip)).drawAtPoint(CGPoint(x: width - 16, y: minPercipY + yOffset), withAttributes: [NSForegroundColorAttributeName:percipitationColor, NSFontAttributeName:font])
            NSString(format: "%.02f", Float(minPercipAndAThird)).drawAtPoint(CGPoint(x: width - 16, y: minPercipAndAThirdY + yOffset), withAttributes: [NSForegroundColorAttributeName:percipitationColor, NSFontAttributeName:font])
            NSString(format: "%.02f", Float(minPercipAndTwoThirds)).drawAtPoint(CGPoint(x: width - 16, y: minPercipAndTwoThirdsY + yOffset), withAttributes: [NSForegroundColorAttributeName:percipitationColor, NSFontAttributeName:font])
            NSString(format: "%.02f", Float(maxPercip)).drawAtPoint(CGPoint(x: width - 16, y: maxPercipY + yOffset), withAttributes: [NSForegroundColorAttributeName:percipitationColor, NSFontAttributeName:font])
            
            // draw histogram
            CGContextSetRGBFillColor(context, percipitationColorComponents[0], percipitationColorComponents[1], percipitationColorComponents[2], percipitationColorComponents[3])
            for (var i = 8 - percip.count; i < 8; i++) {
                let x:CGFloat = xOffset + step * (CGFloat(i))
                let curPercip = CGFloat(percip[i - 8 + percip.count])
                let perc = curPercip / deltaPercip
                let curPercipY:CGFloat = minPercipY + perc * deltaPercipY
                
                CGContextBeginPath(context)
                CGContextMoveToPoint(context, x - 8, curPercipY)
                CGContextAddLineToPoint(context, x + 8, curPercipY)
                CGContextAddLineToPoint(context, x + 8, minPercipY)
                CGContextAddLineToPoint(context, x - 8, minPercipY)
                CGContextFillPath(context)
            }
        } else {
            NSString(string: "0").drawAtPoint(CGPoint(x: width - 12, y: graphHeight - 6 - 6), withAttributes: [NSForegroundColorAttributeName:percipitationColor, NSFontAttributeName:font])
            NSString(string: "0").drawAtPoint(CGPoint(x: width - 12, y: 3 - 6), withAttributes: [NSForegroundColorAttributeName:percipitationColor, NSFontAttributeName:font])
        }
        
        //////////////////////
        // DRAW TEMPERATURE //
        //////////////////////
        // calculations - temperatures
        let sortedTemp:[CGFloat] = temp.sorted({(t1:CGFloat, t2:CGFloat) -> Bool in return t1 < t2})
        var minTemp = sortedTemp.first! - 2
        minTemp -= minTemp % 2
        var maxTemp = sortedTemp.last! + 2
        maxTemp -= maxTemp % 2
        let deltaTemp = maxTemp - minTemp
        let minTempAndAThird = minTemp + (maxTemp - minTemp) / 3
        let minTempAndTwoThirds = minTemp + (maxTemp - minTemp) / 3 * 2
        
        // calculations - graph coordinates
        let minTempY:CGFloat = graphHeight - 6
        let maxTempY:CGFloat = 3
        let deltaTempY = maxTempY - minTempY
        let minTempAndAThirdY = minTempY + (maxTempY - minTempY) / 3
        let minTempAndTwoThirdsY = minTempY + (maxTempY - minTempY) / 3 * 2
        
        // draw scale
        let yOffset:CGFloat = -6 // offset to center text on y-coordinate
        NSString(format: "%.00f", Float(minTemp)).drawAtPoint(CGPoint(x: 0, y: minTempY + yOffset), withAttributes: [NSForegroundColorAttributeName:tempColor, NSFontAttributeName:font])
        NSString(format: "%.00f", Float(minTempAndAThird)).drawAtPoint(CGPoint(x: 0, y: minTempAndAThirdY + yOffset), withAttributes: [NSForegroundColorAttributeName:tempColor, NSFontAttributeName:font])
        NSString(format: "%.00f", Float(minTempAndTwoThirds)).drawAtPoint(CGPoint(x: 0, y: minTempAndTwoThirdsY + yOffset), withAttributes: [NSForegroundColorAttributeName:tempColor, NSFontAttributeName:font])
        NSString(format: "%.00f", Float(maxTemp)).drawAtPoint(CGPoint(x: 0, y: maxTempY + yOffset), withAttributes: [NSForegroundColorAttributeName:tempColor, NSFontAttributeName:font])
        
        // draw lines
        CGContextSetLineDash(context, 0, nil, 0)
        CGContextSetLineWidth(context, 3)
        CGContextSetRGBStrokeColor(context, tempColorComponents[0], tempColorComponents[1], tempColorComponents[2], tempColorComponents[3])
        CGContextMoveToPoint(context, xOffset + step * (CGFloat(8 - temp.count)), minTempY + (CGFloat(temp[0]) - minTemp) / deltaTemp * deltaTempY)
        for (var i = 8 - temp.count + 1; i < 8; i++) {
            let x:CGFloat = xOffset + step * (CGFloat(i))
            let curTemp = CGFloat(temp[i - 8 + temp.count])
            let perc = (curTemp - minTemp) / deltaTemp
            let curTempY:CGFloat = minTempY + perc * deltaTempY
            
            CGContextAddLineToPoint(context, x, curTempY)
        }
        CGContextStrokePath(context)
    }
}
