//
//  SettingsViewController.swift
//  WeatherApplication
//
//  Created by Fabian Furger on 11/23/14.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    @IBAction func temperatureUnitChanged(sender: UISegmentedControl) {

        model.setTemperatureUnit(TemperatureUnit(rawValue: temperatureUnits.titleForSegmentAtIndex(temperatureUnits.selectedSegmentIndex)!)!)
    }
    
    @IBOutlet weak var temperatureUnits: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        switch (model.getTemperatureUnit()) {
        case .Celsius:
            temperatureUnits.selectedSegmentIndex = 0
            break
        case .Fahrenheit:
            temperatureUnits.selectedSegmentIndex = 1
            break
        case .Kelvin:
            temperatureUnits.selectedSegmentIndex = 2
            break
        default:
            println("ERROR: UNKNOWN TEMPERATURE: \(model.getTemperatureUnit().rawValue)")
            break
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
