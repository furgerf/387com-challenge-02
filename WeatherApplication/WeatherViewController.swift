//
//  WeatherViewController.swift
//  WeatherApplication
//
//  Created by Fabian Furger on 30/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherViewController: UIViewController, UIGestureRecognizerDelegate, UITableViewDataSource, UITableViewDelegate, CLLocationManagerDelegate {
    ///////////////
    // VARIABLES //
    ///////////////
    var selectedSection = -1
    let colorYellow = UIColor(red: 1, green: 1, blue: 0.75*1, alpha: 1).CGColor
    let colorBlue = UIColor(red: 0.75*1, green: 0.75*1, blue: 1, alpha: 1).CGColor
    let gradientLayer = CAGradientLayer()
    let location = CLLocationManager()
    
    /////////////////////////
    // OUTLETS AND ACTIONS //
    /////////////////////////
    // navigation bar
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var navigationTitle: UINavigationItem!
    
    // table
    @IBOutlet weak var weatherTable: UITableView!
    
    // gestures
    @IBOutlet var swipeRight: UISwipeGestureRecognizer!
    @IBAction func swipeRightGesture(sender: UISwipeGestureRecognizer) {
        if self.pageIndicator.currentPage == 0 {
            return;
        }
        
        self.moveToPage(self.pageIndicator.currentPage - 1)
    }
    @IBOutlet var swipeLeft: UISwipeGestureRecognizer!
    @IBAction func swipeLeftGesture(sender: UISwipeGestureRecognizer) {
        if self.pageIndicator.currentPage == self.pageIndicator.numberOfPages - 1 {
            return;
        }
    
        self.moveToPage(self.pageIndicator.currentPage + 1)
    }
    
    // page indicator
    @IBOutlet weak var pageIndicator: UIPageControl!
 
    // add-button
    @IBAction func addLocation(sender: UIBarButtonItem) {
        // instantiate alert
        var alert = UIAlertController(title: "New Location", message: "How do you want to add the new location?", preferredStyle: UIAlertControllerStyle.Alert)
        // add actions
        alert.addAction(UIAlertAction(title: "Add manually", style: UIAlertActionStyle.Default, handler: {
            (action) -> Void in
            
            var nextAlert = UIAlertController(title: "Manual Location", message: "Type in city and country", preferredStyle: UIAlertControllerStyle.Alert)
            
            var cityField:UITextField?
            var countryField:UITextField?
            // add text fields
            nextAlert.addTextFieldWithConfigurationHandler({
                (textField: UITextField!) -> Void in
                textField.placeholder = "City"
                cityField = textField
            })
            nextAlert.addTextFieldWithConfigurationHandler({
                (textField: UITextField!) -> Void in
                textField.placeholder = "Country code"
                countryField = textField
            })
            nextAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: {
                (action) -> Void in
                
                // executed when currently entered data should be added
                if let city = cityField?.text {
                    if let country = countryField?.text {
                        if !city.isEmpty && !country.isEmpty {
                            // if both fields contain data, try and add location
                            self.addLocation(city, countryCode: country)
                            return
                        }
                    }
                }
                
                // notify user that not all fields were filled out
                var otherAlert = UIAlertController(title: "Empty fields", message: "Please fill out all fields", preferredStyle: UIAlertControllerStyle.Alert)
                otherAlert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (action) -> Void in self.presentViewController(alert, animated: true, completion: nil) }))
                self.presentViewController(otherAlert, animated: true, completion: nil)
            }))
            self.presentViewController(nextAlert, animated: true, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "Current location", style: UIAlertActionStyle.Default, handler: {
            (action) -> Void in
            self.addCurrentLocation()
        }))
        alert.addAction(UIAlertAction(title: "Favorites", style: UIAlertActionStyle.Default, handler: {
            (action) -> Void in
            // executed when location from favorites should be added
            var otherAlert = UIAlertController(title: "Favorites", message: "Chose one of your favorites", preferredStyle: UIAlertControllerStyle.Alert)
            // add one alert per bookmarked location
            let bookmarks = model.getPersistedBookmarks()
            for (var i = 0; i < bookmarks.count; i++) {
                otherAlert.addAction(UIAlertAction(title: "\(bookmarks[i].0), \(bookmarks[i].1)", style: UIAlertActionStyle.Default, handler: {
                    (action) -> Void in
                    //let closureI = i
                    let bm = model.getPersistedBookmarks()
                    for (var closureI = 0; closureI < bm.count; closureI++) {
                        if (action.title == "\(bm[closureI].0), \(bm[closureI].1)") {
                            self.addLocation(bookmarks[closureI].0, countryCode: bookmarks[closureI].1)
                            break
                        }
                    }
                }))
            }
            // add cancel-button
            otherAlert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(otherAlert, animated: true, completion: nil)
        }))
        // add cancel-button
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    // remove-button
    @IBAction func removeLocation(sender: UIBarButtonItem) {
        model.removeLocationAt(self.pageIndicator.currentPage, persist: true)
    }
    
    // bookmark-button
    @IBAction func bookmarkLocation(sender: UIBarButtonItem) {
        // try to add bookmark and notify user of result
        if (model.addBookmarkAt(self.pageIndicator.currentPage)) {
            // bookmark added
            var alert = UIAlertController(title: "Bookmark added", message: "The location has been bookmarked!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            // bookmark already exists
            var alert = UIAlertController(title: "Already bookmarked", message: "This location is already bookmarked!", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    @IBAction func refreshData(sender: UIBarButtonItem) {
        UIView.animateWithDuration(1, animations: {
            self.weatherTable.alpha = 0
            self.navigationBar.alpha = 0
            }, completion: {(foo) -> Void in
                self.loadWeatherData()
        })
    }
    
    @IBAction func returnFromSettings(segue: UIStoryboardSegue) {
        self.weatherTable.reloadData()
    }
    
    
    /////////////////
    // OWN METHODS //
    /////////////////
    func addCurrentLocation() {
        self.location.startUpdatingLocation()
    }
    
    func addLocation(city: String, countryCode: String) {
        // overload method
        return addLocation(city, countryCode: countryCode, persist: true, async: true)
    }
    
    func addLocation(city: String, countryCode: String, persist: Bool, async: Bool) {
        // turn network indicator on
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        // actual task to be completed here
        var addLocationBlock = { model.addCityWeather(city, country: countryCode, persist: persist) }
        
        // carry out task (a-)synchronously
        if (async) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                addLocationBlock()
            }
        } else {
            addLocationBlock()
        }
    }
    
    func moveToPage(page: Int) {
        if page == 0 && model.dailyWeatherData.count == 0 {
            return
        }
        
        self.selectedSection = -1
        
        // smooth transistion between pages
        UIView.animateWithDuration(1, animations: {
            self.weatherTable.alpha = 0
            self.navigationBar.alpha = 0
            }, completion: {
                (value: Bool) in
                self.pageIndicator.currentPage = page
                self.navigationTitle.title = model.dailyWeatherData[self.pageIndicator.currentPage].city + ", " + model.dailyWeatherData[self.pageIndicator.currentPage].country
                self.weatherTable.reloadData()
                UIView.animateWithDuration(1, animations: {
                    self.weatherTable.alpha = 1
                    self.navigationBar.alpha = 1
                })
        })
    }

    func loadWeatherData() {
        // ensure no completion is executed
        model.weatherUpdateCompletion = {(value) -> Void in}
        
        // store page position
        let page = self.pageIndicator.currentPage
        
        // remove all existing weather data
        model.clearData(false)
        
        // load weather data - BEFORE completion is set
        let locations:[(String, String)] = model.getPersistedLocations()
        for (var i = 0; i < locations.count; i++) {
            addLocation(locations[i].0, countryCode: locations[i].1, persist: false, async: false)
        }
        
        // assign completion handler
        model.weatherUpdateCompletion = {
            (success: Bool, page: Int) -> Void in
            dispatch_async(dispatch_get_main_queue()) {
                // turn network indicator off and move to new page whenever one is added
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                if success {
                    // move to page
                    self.pageIndicator.numberOfPages = model.dailyWeatherData.count
                    self.moveToPage(page)
                } else {
                    // notify user
                    var alert = UIAlertController(title: "Location not found", message: "The location could not be found. Maybe your internet connection is not working?", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
        }
        
        // run completion to display retrieved data
        if model.dailyWeatherData.count == 0 {
            dispatch_async(dispatch_get_main_queue(), {
                self.addLocation(UIBarButtonItem())
            })
        } else {
            model.weatherUpdateCompletion(true, page)
        }
    }
    
    ////////////////////////
    // SUPERCLASS METHODS //
    ////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // do delegate stuff
        self.weatherTable.delegate = self
        self.weatherTable.dataSource = self
        self.view.addGestureRecognizer(self.swipeLeft)
        self.view.addGestureRecognizer(self.swipeRight)
        
        // prepare "blank" views
        self.pageIndicator.currentPage = 0
        self.pageIndicator.numberOfPages = 0
        self.weatherTable.alpha = 0
        self.navigationBar.alpha = 0
        self.weatherTable.backgroundColor = UIColor.clearColor()
        
        // prepare gradient background
        self.gradientLayer.colors = [self.colorYellow, self.colorBlue]
        self.gradientLayer.startPoint = CGPointMake(0, 0)
        self.gradientLayer.endPoint = CGPointMake(0, 1)
        self.view.backgroundColor = UIColor.clearColor()
        let backgroundLayer = gradientLayer
        backgroundLayer.frame = view.frame
        self.view.layer.insertSublayer(backgroundLayer, atIndex: 0)
        
        // CL delegate
        CLLocationManager.locationServicesEnabled()
        self.location.requestWhenInUseAuthorization()
        self.location.delegate = self
        self.location.distanceFilter = kCLDistanceFilterNone
        self.location.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        self.loadWeatherData()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // as many sections as days for which we got ddata
        return model.dailyWeatherData.isEmpty ? 0 : model.dailyWeatherData[self.pageIndicator.currentPage].date.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // one row per section
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("WeatherCell", forIndexPath: indexPath) as WeatherCell
        
        let dailyDate = model.dailyWeatherData[self.pageIndicator.currentPage].date[indexPath.section]
        var hourlyStartIndex = -1

        var section = indexPath.section
        var row = indexPath.row
        
        // find start index for (partial) hourly data
        while (model.hourlyWeatherData[self.pageIndicator.currentPage].date[++hourlyStartIndex].rangeOfString(dailyDate) == nil) {
            if hourlyStartIndex == model.hourlyWeatherData[self.pageIndicator.currentPage].date.count - 1 {
                // day doesnt exist in hourly data
                return cell
            }
        }
        
        // find end index for (partial) hourly data
        var hourlyEndIndex = hourlyStartIndex - 1
        while (model.hourlyWeatherData[self.pageIndicator.currentPage].date[++hourlyEndIndex].rangeOfString(dailyDate) != nil) {
            if hourlyEndIndex == model.hourlyWeatherData[self.pageIndicator.currentPage].date.count - 1 {
                // we reached the end of the array
                hourlyEndIndex++
                break
            }
        }
        
        // feed data to the custom cell
        cell.displayWeatherData(model.dailyWeatherData[self.pageIndicator.currentPage], dailyIndex: indexPath.section, hourlyWeather: model.hourlyWeatherData[self.pageIndicator.currentPage], hourlyStartIndex: hourlyStartIndex, hourlyEndIndex: hourlyEndIndex)
        
        cell.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
        
        return cell
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var header = UITableViewCell(frame: CGRectMake(0, 0, tableView.bounds.width, 30))
        header.backgroundColor = UIColor(red: 0.7, green: 0.7, blue: 0.7, alpha: 0.3)
        header.textLabel.text = model.dailyWeatherData[self.pageIndicator.currentPage].date[section]
        return header
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // table cell selection stuff
        if self.selectedSection == indexPath.section {
            self.selectedSection = -1
        } else {
            self.selectedSection = indexPath.section
        }
        
        self.weatherTable.beginUpdates()
        self.weatherTable.endUpdates()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // adjust row height of (de-)selected rows
        return indexPath.section == self.selectedSection ? weatherCellHeightSelected : weatherCellHeightUnselected
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
        self.location.stopUpdatingLocation()
        println("New location: " + locations.description)
        
        if let loc = locations.last as? CLLocation {
            let coords = loc.coordinate
            
            // turn network indicator on
            UIApplication.sharedApplication().networkActivityIndicatorVisible = true
            // carry out task (a-)synchronously
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                model.addCoordinateWeather(coords.latitude, lon: coords.longitude, persist: true)
            }
        }
    }
    
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
        self.location.stopUpdatingLocation()

        var alert = UIAlertController(title: "Location not found", message: "Current location could not be determined.", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        // turn network indicator off
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
}

