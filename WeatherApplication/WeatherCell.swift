//
//  WeatherCell.swift
//  WeatherApplication
//
//  Created by Fabian Furger on 11/14/14.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

// global access
var weatherCellHeightSelected:CGFloat = -1
var weatherCellHeightUnselected:CGFloat = -1

class WeatherCell: UITableViewCell {
    ///////////////
    // VARIABLES //
    ///////////////
    var selectedCount = 0

    /////////////////////////
    // OUTLETS AND ACTIONS //
    /////////////////////////
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var weatherDescription: UILabel!
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var weatherGraph: GraphView!
    
    @IBOutlet weak var graphHeight: NSLayoutConstraint!
    @IBOutlet weak var labelHeight: NSLayoutConstraint!
    
    ////////////////////////
    // SUPERCLASS METHODS //
    ////////////////////////
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // set (un-)selected cell heights - can only do that once a cell is created
        if weatherCellHeightSelected == -1 {
            weatherCellHeightSelected = weatherGraph.frame.origin.y + weatherGraph.frame.height
        }
        if weatherCellHeightUnselected == -1 {
            weatherCellHeightUnselected = label1.frame.origin.y + label1.frame.height
        }
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // update selected count
        if selected {
            self.selectedCount++
        } else {
            self.selectedCount = 0
        }
        
        // the cell is selected if the amount of consecutive selection taps is odd
        self.graphHeight.constant = self.selectedCount % 2 == 0 ? 0 : 131
        self.labelHeight.constant = self.selectedCount % 2 == 0 ? 0 : 21
    }
    
    func displayWeatherData(dailyWeather: DailyWeather, dailyIndex: Int, hourlyWeather: HourlyWeather, hourlyStartIndex: Int, hourlyEndIndex: Int) {
        // set varios texts
        self.weatherDescription.text = dailyWeather.weatherDescription[dailyIndex]
        
        var percip = dailyWeather.rain[dailyIndex] + dailyWeather.snow[dailyIndex]
        var minTemp = String(format: "%.02f", arguments: [model.getConvertedTemperature(dailyWeather.minTemp[dailyIndex])])
        var maxTemp = String(format: "%.02f", arguments: [model.getConvertedTemperature(dailyWeather.maxTemp[dailyIndex])])
        
        self.label1.text = "\(minTemp) - \(maxTemp)°\(model.getTemperatureUnitLetter())\t\tpercip: \(percip)mm/3h"
        self.label2.text = "Clouds: \(dailyWeather.clouds[dailyIndex])%\tHumidity: \(dailyWeather.humidity[dailyIndex])%\tPressure: \(dailyWeather.pressure[dailyIndex])hPa"
        
        // set icon
        self.weatherIcon.image = dailyWeather.weatherIcon[dailyIndex]
        
        // perpare hourly data for the graph
        var hourlyTime = [String]()
        var hourlyTemp = [CGFloat]()
        var hourlyPercip = [CGFloat]()
        for (var i = hourlyStartIndex; i < hourlyEndIndex; i++) {
            var dateString = hourlyWeather.date[i] as NSString
            var start = -1
            while (dateString.substringWithRange(NSMakeRange(++start, 1)) != "(") {}
            hourlyTime.append(dateString.substringWithRange(NSMakeRange(++start, 5)))
            hourlyTemp.append(CGFloat(model.getConvertedTemperature((hourlyWeather.temp[i] + hourlyWeather.minTemp[i] + hourlyWeather.maxTemp[i]) / 3)))
            hourlyPercip.append(CGFloat(hourlyWeather.rain[i] + hourlyWeather.snow[i]))
        }
        
        // feed data to the graph
        self.weatherGraph.time = hourlyTime
        self.weatherGraph.temp = hourlyTemp
        self.weatherGraph.percip = hourlyPercip
    }
}
