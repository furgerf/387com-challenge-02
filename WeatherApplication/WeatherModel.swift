//
//  WeatherModel.swift
//  WeatherApplication
//
//  Created by Fabian Furger on 13/10/2014.
//  Copyright (c) 2014 Coventry University. All rights reserved.
//

import UIKit

/////////////
// STRUCTS //
/////////////
struct DailyWeather {
    let city:String
    let country:String
    
    let date:[String]
    
    let weatherIcon:[UIImage]
    let weatherDescription:[String]
    
    let wind:[Float]        // mps
    let clouds:[Int]        // %
    let humidity:[Int]      // %
    let pressure:[Float]    // hPa
    let rain:[Float]        // mm in last 3h
    let snow:[Float]        // mm in last 3h
    
    let minTemp:[Float]     // °K
    let maxTemp:[Float]     // °K
    let dayTemp:[Float]     // °K
    let nightTemp:[Float]   // °K
    let eveTemp:[Float]     // °K
    let morningTemp:[Float] // °K
}
struct HourlyWeather {
    let city:String
    
    let date:[String]
    
    let weatherDescription:[String]
    
    let wind:[Float]        // mps
    let clouds:[Int]        // %
    let humidity:[Int]      // %
    let pressure:[Float]    // hPa
    let rain:[Float]        // mm in last 3h
    let snow:[Float]        // mm in last 3h
    
    let temp:[Float]        // °K
    let minTemp:[Float]     // °K
    let maxTemp:[Float]     // °K
}

///////////
// ENUMS //
///////////
enum TemperatureUnit: String {
    case Celsius = "Celsius"
    case Fahrenheit = "Fahrenheit"
    case Kelvin = "Kelvin"
}

///////////////////////////
// GLOBAL MODEL INSTANCE //
///////////////////////////
let model:WeatherModel = WeatherModel()

class WeatherModel {
    // API access
    let urlCityHourly:String = "http://api.openweathermap.org/data/2.5/forecast?q=CITY,COUNTRY&mode=json"
    let urlCityDaily:String = "http://api.openweathermap.org/data/2.5/forecast/daily?q=CITY,COUNTRY&mode=json"
    let urlCoordHourly:String = "http://api.openweathermap.org/data/2.5/forecast?lat=LATITUDE&lon=LONGITUDE&mode=json"
    let urlCoordDaily:String = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=LATITUDE&lon=LONGITUDE&mode=json"
    let urlIcons:String = "http://openweathermap.org/img/w/"
    
    // completion handler to be called on weather update
    var weatherUpdateCompletion:(Bool, Int) -> Void = {(Bool, Int) -> Void in }
    
    // runtime data
    var dailyWeatherData = [DailyWeather]()
    var hourlyWeatherData = [HourlyWeather]()
    
    
    // data persistence
    let DATA_CURRENT = "current"
    let DATA_BOOKMARKS = "bookmarks"
    let DATA_TEMPERATURE_UNIT = "temperature unit"
    let savedData:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    
    // data persistence - read access
    func getPersistedLocations() -> [(String, String)] {
        return self.getPersistedData(DATA_CURRENT)
    }
    func getPersistedBookmarks() -> [(String, String)] {
        return self.getPersistedData(DATA_BOOKMARKS)
    }
    func getPersistedData(key: String) -> [(String, String)] {
        var locations = [(String, String)]()
        
        // convert from string-array to tuple-array
        if let data = self.savedData.stringArrayForKey(key) as? [String] {
            for (var i = 0; i < data.count; i++) {
                let temp = data[i].componentsSeparatedByString(";")
                locations.append((temp[0], temp[1]))
            }
        }
        
        return locations
    }
    func getTemperatureUnit() -> TemperatureUnit {
        return TemperatureUnit(rawValue: self.savedData.stringForKey(DATA_TEMPERATURE_UNIT) ?? TemperatureUnit.Celsius.rawValue)!
    }
    
    // data persistence - write access
    func persistLocations(locations: [(String, String)]) {
        self.persistData(DATA_CURRENT, locations: locations)
    }
    func persistBookmarks(bookmarks: [(String, String)]) {
        self.persistData(DATA_BOOKMARKS, locations: bookmarks)
    }
    func persistData(key: String, locations: [(String, String)]) {
        // convert from tuple-array to string-array
        var data = [String]()
        for (var i = 0; i < locations.count; i++) {
            data.append("\(locations[i].0);\(locations[i].1)")
        }
        
        // store string-array
        self.savedData.setObject(data, forKey: key)
        self.savedData.synchronize()
    }
    func setTemperatureUnit(unit: TemperatureUnit) {
        self.savedData.setObject(unit.rawValue, forKey: DATA_TEMPERATURE_UNIT)
        self.savedData.synchronize()
    }
    
    // methods
    func clearData(persist: Bool) {
        while (self.dailyWeatherData.count > 0) {
            self.removeLocationAt(0, persist: persist)
        }
    }
    
    func removeLocationAt(index: Int, persist: Bool) {
        // remove from runtime data
        self.dailyWeatherData.removeAtIndex(index)
        self.hourlyWeatherData.removeAtIndex(index)
        
        if persist {
            // remove from persistent data
            var locations = self.getPersistedLocations()
            locations.removeAtIndex(index)
            self.persistLocations(locations)
        }
        
        self.weatherUpdateCompletion(true, index == 0 ? 0 : index - 1)
    }
    
    func getTemperatureUnitLetter() -> String {
        return String(Array(self.getTemperatureUnit().rawValue)[0])
    }
    
    func getConvertedTemperature(kelvinTemperature: Float) -> Float {
        switch self.getTemperatureUnit() {
        case .Celsius:
            return kelvinTemperature - 273.15
        case .Fahrenheit:
            return 1.8 * (kelvinTemperature - 273.15) + 32
        case .Kelvin:
            return kelvinTemperature
        default:
            println("ERROR: UNKNOWN TEMPERATURE UNIT")
        }
    }
    
    func addBookmarkAt(index: Int) -> Bool {
        var bookmarks = self.getPersistedBookmarks()
        let city = self.dailyWeatherData[index].city
        let country = self.dailyWeatherData[index].country
        
        // ensure city is not yet bookmarked
        for (var i = 0; i < bookmarks.count; i++) {
            if (bookmarks[i].0 == city && bookmarks[i].1 == country) {
                return false
            }
        }
        
        // add and persist bookmark
        bookmarks.append(city, country)
        self.persistBookmarks(bookmarks)
        
        return true
    }
    
    func addCoordinateWeather(lat:Double, lon:Double, persist: Bool) {
        println("daily: " + urlCoordDaily.stringByReplacingOccurrencesOfString("LATITUDE", withString: lat.description, options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("LONGITUDE", withString: lon.description, options: NSStringCompareOptions.LiteralSearch, range: nil))
        
        self.addWeather(urlCoordDaily.stringByReplacingOccurrencesOfString("LATITUDE", withString: lat.description, options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("LONGITUDE", withString: lon.description, options: NSStringCompareOptions.LiteralSearch, range: nil), hourlyUrl: urlCityHourly.stringByReplacingOccurrencesOfString("LATITUDE", withString: lat.description, options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("LONGITUDE", withString: lon.description, options: NSStringCompareOptions.LiteralSearch, range: nil), persist: persist)
    }
    
    func addCityWeather(city:String, country: String, persist: Bool) {
        let cityFixed = city.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil)
        let countryFixed = country.stringByReplacingOccurrencesOfString(" ", withString: "_", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        self.addWeather(urlCityDaily.stringByReplacingOccurrencesOfString("CITY", withString: cityFixed, options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("COUNTRY", withString: countryFixed, options: NSStringCompareOptions.LiteralSearch, range: nil), hourlyUrl: urlCityHourly.stringByReplacingOccurrencesOfString("CITY", withString: cityFixed, options: NSStringCompareOptions.LiteralSearch, range: nil).stringByReplacingOccurrencesOfString("COUNTRY", withString: countryFixed, options: NSStringCompareOptions.LiteralSearch, range: nil), persist: persist)
    }
    
    func addWeather(dailyUrl: String, hourlyUrl: String, persist: Bool) {
        // retrieve daily data
        var dailyData = parseJSON(getJSON(dailyUrl))
    
        if dailyData["cod"] as NSString != "200" {
            self.weatherUpdateCompletion(false, 0)
            return
        }
        
        // retrieve hourly data
        var hourlyData = parseJSON(getJSON(hourlyUrl))
        
        // error during data retrieval, call completion handler
        if hourlyData["cod"] as NSString != "200" {
            self.weatherUpdateCompletion(false, 0)
            return
        }
        
        // both types of data were successfully retrieved so add them to the runtime data
        self.dailyWeatherData.append(processDailyData(dailyData))
        self.hourlyWeatherData.append(processHourlyData(hourlyData))
        
        // if new list of locations should be persisted, do so
        // (this typically happens every time except on startup to avoid duplicates)
        if (persist) {
            var locations = self.getPersistedLocations()
            locations.append((self.dailyWeatherData.last!.city, self.dailyWeatherData.last!.country))
            self.persistLocations(locations)
        }
        
        // adding city finished, call completion handler
        self.weatherUpdateCompletion(true, self.dailyWeatherData.count - 1)
    }
    
    private func getJSON(urlToRequest: String) -> NSData{
        return NSData(contentsOfURL: NSURL(string: urlToRequest)!)!
    }
    
    private func parseJSON(inputData: NSData) -> NSDictionary{
        var error: NSError?
        var boardsDictionary: NSDictionary = NSJSONSerialization.JSONObjectWithData(inputData, options: NSJSONReadingOptions.MutableContainers, error: &error) as NSDictionary
        return boardsDictionary
    }
    
    
    // CAUTION: DO NOT SCROLL DOWN, SERIOUS EYE INJURY MAY OCCUR...
    private func processDailyData(data:NSDictionary) -> DailyWeather {
        if data["cod"] as String != "200" {
            println("ERROR: COULD NOT RETRIEVE DATA")
        }
        
        let city = (data["city"] as NSDictionary)["name"] as String
        let country = (data["city"] as NSDictionary)["country"] as String
        
        var date = [String]()
        
        var icon = [UIImage]()
        var description = [String]()
        
        var wind = [Float]()
        var clouds = [Int]()
        var humidity = [Int]()
        var pressure = [Float]()
        var rain = [Float]()
        var snow = [Float]()
        
        var tempMin = [Float]()
        var tempMax = [Float]()
        var tempDay = [Float]()
        var tempNight = [Float]()
        var tempEve = [Float]()
        var tempMorning = [Float]()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        
        let dailyData = data.valueForKey("list") as NSArray
        for (index, dayData) in enumerate(dailyData) {
            let day = dayData as NSDictionary
            let weather = (day["weather"] as NSArray)[0] as NSDictionary
            let temp = day["temp"] as NSDictionary
            
            date.append(dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: day["dt"] as NSTimeInterval)))
            
            icon.append(UIImage(data: NSData(contentsOfURL: NSURL(string: urlIcons + (weather["icon"] as String) + ".png")!)!)!)
            description.append((weather["main"] as String) + " (" + (weather["description"] as String) + ")")
            
            wind.append(day["speed"] as Float)
            clouds.append(day["clouds"] as Int)
            humidity.append(day["humidity"] as Int)
            pressure.append(day["pressure"] as Float)
            rain.append(day["rain"] == nil ? 0 : (day["rain"] as Float!))
            snow.append(day["snow"] == nil ? 0 : (day["snow"] as Float!))
            
            tempMin.append(temp["min"] as Float)
            tempMax.append(temp["max"] as Float)
            tempDay.append(temp["day"] as Float)
            tempNight.append(temp["night"] as Float)
            tempEve.append(temp["eve"] as Float)
            tempMorning.append(temp["morn"] as Float)
        }
        
        return DailyWeather(city: city, country: country, date: date, weatherIcon: icon, weatherDescription: description, wind: wind, clouds: clouds, humidity: humidity, pressure: pressure, rain: rain, snow: snow, minTemp: tempMin, maxTemp: tempMax, dayTemp: tempDay, nightTemp: tempNight, eveTemp: tempEve, morningTemp: tempMorning)
    }
    
    private func processHourlyData(data:NSDictionary) -> HourlyWeather {
        if data["cod"] as String != "200" {
            println("ERROR: COULD NOT RETRIEVE DATA")
        }
        
        let city = ((data["city"] as NSDictionary)["name"] as String) + ", " + ((data["city"] as NSDictionary)["country"] as String)
        
        var date = [String]()
        
        var description = [String]()
        
        var wind = [Float]()
        var clouds = [Int]()
        var humidity = [Int]()
        var pressure = [Float]()
        var rain = [Float]()
        var snow = [Float]()
        
        var temp = [Float]()
        var tempMin = [Float]()
        var tempMax = [Float]()
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy (HH:mm)"
        
        let dailyData = data.valueForKey("list") as NSArray
        for (index, dayData) in enumerate(dailyData) {
            let day = dayData as NSDictionary
            let weather = (day["weather"] as NSArray)[0] as NSDictionary
            let tmp = day["main"] as NSDictionary
            
            date.append(dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: day["dt"] as NSTimeInterval)))
            
            description.append((weather["main"] as String) + " (" + (weather["description"] as String) + ")")
            
            wind.append((day["wind"] as NSDictionary)["speed"] as Float)
            clouds.append((day["clouds"] as NSDictionary)["all"] as Int)
            humidity.append(tmp["humidity"] as Int)
            pressure.append(tmp["pressure"] as Float)
            rain.append((day["rain"] == nil ? 0 : (day["rain"] as NSDictionary)["3h"] as Float))
            snow.append((day["snow"] == nil ? 0 : (day["snow"] as NSDictionary)["3h"] as Float))
            temp.append(tmp["temp"] as Float)
            tempMin.append(tmp["temp_min"] as Float)
            tempMax.append(tmp["temp_max"] as Float)
        }
        
        return HourlyWeather(city: city, date: date, weatherDescription: description, wind: wind, clouds: clouds, humidity: humidity, pressure: pressure, rain: rain, snow: snow, temp: temp, minTemp: tempMin, maxTemp: tempMax)
    }
}
